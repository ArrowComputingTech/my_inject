#!/home/hz/.rbenv/shims/ruby

module Enumerable
  def my_inject(*arg)
    memo, *x = self
    self.each do |x|
      memo = yield(memo, x)
    end
    memo
  end
end

def multiply_els(arr)
  sum = 1
  arr.each do |x|
    sum *= x
  end
  sum
end

arr = [3,4,5,6,7]
puts "my_inject--"
puts arr.my_inject { |sum,x| sum + x }
puts "multiply_els--"
puts multiply_els(arr)
